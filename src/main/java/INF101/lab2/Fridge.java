package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    
    int  max_size = 20;
    List<FridgeItem> Fridge = new ArrayList<FridgeItem>();

    public int totalSize(){
        return max_size;

    }

    @Override
    public int nItemsInFridge() {
        return Fridge.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (Fridge.size() == max_size){
            return false;
        }
        Fridge.add(item);
        return true;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (Fridge.size() > 0){
            Fridge.remove(item);
        }else{
            throw new NoSuchElementException();
        }
        
    }

    @Override
    public void emptyFridge() {
        Fridge.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expItems = new ArrayList<FridgeItem>();
        for (FridgeItem item : Fridge){
            if (item.hasExpired()){
                expItems.add(item);
            }
        }
        for (FridgeItem expired : expItems){
                Fridge.remove(expired);

        }
        
        return expItems;
    }
}
